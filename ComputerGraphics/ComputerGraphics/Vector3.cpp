#include "Vector3.h"
#include <math.h>

namespace sun_graphics
{
	#define sqrt_data_type sqrtf

	void Vector3::invert()
	{
		x = -x;
		y = -y;
		z = -z;
	}

	Vector3 Vector3::operator=(const Vector3 some_vector)
	{
		x = some_vector.x;
		y = some_vector.y;
		z = some_vector.z;

		return *this;
	}

	Vector3 operator*(const data_type value, const Vector3 some_vector)
	{
		return Vector3(value*some_vector.x, value*some_vector.y, value*some_vector.z);
	}

	void Vector3::operator*=(const data_type value)
	{
		x *= value;
		y *= value;
		z *= value;

	}

	Vector3 Vector3::operator*(const data_type value) const
	{
		return Vector3(value*x, value*y, value*z);
	}


	data_type Vector3::magnitude() const
	{
		return sqrt_data_type(square_magnitude());
	}

	data_type Vector3::square_magnitude() const
	{
		return x*x + y*y + z*z;
	}

	void Vector3::normalize()
	{
		data_type l = magnitude();
		//�ų����������
		if (l > 0)
		{
			(*this) *= ((data_type)1 / l);
		}
	}
}


