#include <GL/glut.h>
#include "Vector3.h";
using namespace sun_graphics;

void myInit(void)
{
	glClearColor(1.0, 1.0, 1.0, 0.0);/* select clearing color  */
	glColor3f(0.0f, 0.0f, 0.0f);//set draw color
	glPointSize(1.0);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0.0, 640.0, 0.0, 480.0);/* initialize viewing values  */
}

void myDisplay(void)
{
	glClear(GL_COLOR_BUFFER_BIT);/* clear all pixels  */
	glBegin(GL_POINTS);
	glVertex2i(100,50);
	glVertex2i(100,130);
	glVertex2i(150,130);
	glEnd();
	glFlush();/* start processing buffered OpenGL routines  */
}



int main(int argc, char** argv)
{
	Vector3 one(1,1,1);
	Vector3 two(2,2,2);

	Vector3 three = one * 3;
	Vector3 four = 4 * one;

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);/*Declare initial display mode(single buffer and RGBA).*/
	glutInitWindowSize(640, 480); /*Declare initial window size.*/
	glutInitWindowPosition(100, 150);/*Declare initial window position.*/
	glutCreateWindow("give you three points");/*Open window with "hello"in its title bar.*/
	glutDisplayFunc(myDisplay); /*Register callback function to display graphics.*/
	myInit();/*Call initialization routines.*/
	glutMainLoop();/*Enter main loop and process events.*/
	//return 0;   /* ANSI C requires main to return int. */
}