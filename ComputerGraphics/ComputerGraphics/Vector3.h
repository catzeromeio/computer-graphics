#pragma once
namespace sun_graphics
{
	typedef float data_type;

	class Vector3
	{
	public:

		data_type x;
		data_type y;
		data_type z;

		Vector3() :x(0), y(0), z(0){};
		Vector3(data_type x, data_type y, data_type z):x(x), y(y), z(z){};
		
		//flips all the components of the vector
		void invert();
		~Vector3(){};

		Vector3 operator*(const data_type value) const;
		friend Vector3 operator*(const data_type value, const Vector3 some_vector);
		void operator*=(const data_type value);

		Vector3 operator=(const Vector3 some_vector);

		data_type magnitude() const;
		data_type square_magnitude() const;
		void normalize();
	
	};

}

